//
//  ViewController.swift
//  trabalho7pt
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Contato: Codable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var atores : [Contato]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return atores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let ator = atores![indexPath.row]
        
        cell.nomePersonagem.text = ator.name
        cell.nomePersonagem.text = ator.actor
        cell.imageview.kf.setImage(with: URL(string: ator.image))
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestData()
        tableViewB.dataSource = self
    }
    
    @IBOutlet weak var tableViewB: UITableView!
    
    private func requestData(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of:[Contato].self){
            response in
            if let atoresG = response.value{
                self.atores = atoresG
                }
            self.tableViewB.reloadData()
        }
    }
    
   
    
    private func setData(){}
        
    }




